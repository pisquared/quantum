import cython
cimport scipy.linalg.cython_blas as blas
import numpy as np

"""
Type	        Storage size    Value range
char	        1 byte	        -128 to 127 or 0 to 255
unsigned char	1 byte	        0 to 255
signed char	    1 byte	        -128 to 127
int	            2 or 4 bytes	-32,768 to 32,767 or -2,147,483,648 to 2,147,483,647
unsigned int	2 or 4 bytes	0 to 65,535 or 0 to 4,294,967,295
short	        2 bytes	        -32,768 to 32,767
unsigned short	2 bytes     	0 to 65,535
long	        8 bytes	        -9223372036854775808 to 9223372036854775807
unsigned long	8 bytes	        0 to 18446744073709551615
"""
ctypedef unsigned long ULong

@cython.boundscheck(False)
@cython.wraparound(False)
def kron(double[:, ::1] a, double[:, ::1] b):
    """
    Calculates a Kronicker product using C.
    TODO: Crashes on 31 qubits since it overflows the int

         While calculating 31 qbits,  get "negative dimensions" - hint towards "Int overflow":::

        Traceback (most recent call last):
          File "/usr/local/google/home/pisquared/workspace/quantum/load_test.py", line 127, in <module>
            run_load_test(32)
          File "/usr/local/google/home/pisquared/workspace/quantum/load_test.py", line 108, in run_load_test
            m = kron(qbits)
          File "ckron/cythkrn.pyx", line 15, in ckron.cythkrn.kron
            result = np.zeros((i*k, j*l), float)
        ValueError: negative dimensions are not allowed

    """
    cdef int i = a.shape[0]
    cdef int j = a.shape[1]
    cdef int k = b.shape[0]
    cdef int l = b.shape[1]
    cdef int onei = 1
    cdef double oned = 1
    cdef int m, n
    result = np.zeros((i*k, j*l), float)
    cdef double[:, ::1] result_v = result
    for n in range(i):
        for m in range(k):
            blas.dger(&l, &j, &oned, &b[m, 0], &onei, &a[n, 0], &onei, &result_v[m+k*n, 0], &l)
    return result