import matplotlib.pyplot as plt
from lib import *


def plot(t, s):
    fig, ax = plt.subplots()
    ax.plot(t, s)

    ax.set(xlabel='angle', ylabel='result', title='plot')
    ax.grid()

    fig.savefig("test.png")
    plt.show()

# Double-slit, which-way
# https://physics.stackexchange.com/questions/229895/understanding-the-quantum-eraser-from-a-quantum-information-stand-point

# Delayed-choice quantum eraser:
# https://quantumcomputing.stackexchange.com/questions/1568/what-is-the-quantum-circuit-equivalent-of-a-delayed-choice-quantum-eraser

# The matrix
# Λ=eiϕ/2|0⟩⟨0|+e−iϕ/2|1⟩⟨1|
# introduces a phase shift between the two paths (this can e.g. be a
# function of the position on the screen, or the relative length of
# the interferometer arms)

# See note on the post:
# you could use quirk to provide interactive versions of the quantum
# circuits. For example, your first circuit is this
# https://algassert.com/quirk#circuit={%22cols%22:[[%22H%22],[%22Z^t%22],
# [%22H%22],[%22Measure%22]]}
# , while your second is
# https://algassert.com/quirk#circuit=%7B%22cols%22:%5B%5B%22H%22%5D,
# %5B%22%E2%80%A2%22,%22X%22%5D,%5B%22Z%5Et%22%5D,%5B%22H%22%5D%5D%7D
# this.

# Note(pisquared): The difference seems in the Lambda operator which
#                  seems to be actually a [T-Gate](
#                  https://www.quantum-inspire.com/kbase/t-gate/)
#                  In quirk, the Z^t keeps |0><0| while the description
#                  in the post spins |0><0| to oposite degrees which
#                  cancels after application of the second H-gate


def double_slit():
    """
    #
    :return:
    """
    ms = list()
    angles = np.arange(-4 * np.pi, 4 * np.pi, 0.1)
    for theta in angles:
        # Here, the first H puts the qubit in the superposition of both
        # paths/slits -- the states |0⟩ and |1⟩ correspond to the two
        # paths/slits
        phi1 = H.on(s("|0>"))
        phi2 = T(theta).on(phi1)

        # and the second H makes the two paths interfere.
        phi3 = H.on(phi2)

        # If the output qubit is measured in state |0⟩,
        # the interference is constructive, otherwise desctructive.
        #
        # You can easily check that this yields an interference pattern which
        # varies like prob(0)=cos(ϕ)2.

        ms.append(phi3.get_prob(0))
        # ms.append(phi3.measure())
    plot(angles, ms)


def which_way():
    ms = list()
    angles = np.arange(-4 * np.pi, 4 * np.pi, 0.1)
    for theta in angles:
        # Here, the first H puts the qubit in the superposition of both
        # paths/slits -- the states |0⟩ and |1⟩ correspond to the two
        # paths/slits
        phi1 = H.on(s("|0>"))

        # Now imagine that we want to copy the which-way information:
        # Then, what we do is
        q2 = s("|0>")
        phi2 = CNOT.on(phi1 * q2)
        phi3 = T(theta).on(phi2, 0)
        phi4 = H.on(phi3, 0)

        # this is, we copy the which-way information before traversing the
        # interferometer onto qubit c. It is easy to see that this destroys
        # the interference pattern, i.e., prob(0)=1/2.

        ms.append(phi4.get_prob(0))
        # ms.append(phi3.measure())
    plot(angles, ms)


if __name__ == "__main__":
    double_slit()
    which_way()
