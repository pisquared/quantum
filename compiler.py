from lib import s


def int_to_state(i):
    return s("|{}>".format(bin(i)))


if __name__ == "__main__":
    print(int_to_state(42))
