def gen():
    print("start gen")
    for i in range(30):
        print('gen {}'.format(i))
        yield i


def ingest(stream):
    print("start ingest+")
    for number in stream:
        print("injest {}".format(number))
        yield number


def main():
    print("main")
    stream = gen()
    something = ingest(stream)
    for i in something:
        print(i)


if __name__ == "__main__":
    main()
