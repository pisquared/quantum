#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 31 13:37:47 2020

@author: kidiki
"""

import numpy as np
from qiskit import (
    QuantumCircuit,
    execute,
    Aer)
from qiskit.visualization import plot_histogram

from scipy.stats import unitary_group
from collections import defaultdict

from qiskit.quantum_info.random.utils import random_state

# Use Aer's qasm_simulator
simulator = Aer.get_backend('qasm_simulator')


def perform_exp(iterations=3):
    simulator = Aer.get_backend('qasm_simulator')
    all_counts = defaultdict(int)

    for i in range(iterations):
        qc = QuantumCircuit(2, 2)

        q0 = random_state(2)
        q1 = q0

        print(q0)

        # Measure in the Bell's basis
        qc.cx(0, 1)
        qc.h(0)
        #  qc.measure_all()
        qc.measure([0, 1], [0, 1])

        # print(qc)

        job = execute(qc, simulator, shots=1)
        result = job.result()
        # print(result)
        counts = result.get_counts(qc)

        # print(counts.keys())

        for k, v in counts.items():
            all_counts[k] += v

    print("\nTotal counts are:\n")
    for k, v in sorted(all_counts.items(), key=lambda x: x[0]):
        print("{}: {}".format(k, v))


if __name__ == "__main__":
    perform_exp()
