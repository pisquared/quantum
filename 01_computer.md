# Quantum Computer in Python from first principles

1. Reinvent how to multiply matrices
2. Present qbits and gates as matrices
3. Override multiplication for qbits to be tensor multiplication and for gates 
to be matrix (TODO: why??)
4. Test basic operations such as Identity matrix, Pauli gates, creating 
superposition with Hadamard, creating entanglement with CNOT. 
