# Implement Quantum Key Distribution (QKD) in Python

1. You have Alice and Bob who want to communicate, they have a quantum and a
classical channel.
2. There is of course Mallory who can be the standard Man-in-the-middle, 
listening on both channels (with QKD we should be able to at least detect her)
3. Alice and Bob should be able to agree on a One-Time-Pad (OTP) - a series
of bytes that are random. Using this, they can use whatever protocol to 
actually encrypt messages (imagine even ceaser shift where each letter is 
shifted by that many units using the OTP bits, or even simpler - XORing)
4. Alice and Bob are able to agree on an OTP using qbits in two orthogonal 
quantum bases where Bob chooses at random which base to measure and what output 
he got.


## Sources:
* [The Code Book](https://www.amazon.com/Code-Book-Science-Secrecy-Cryptography/dp/0385495323)
* [wiki](https://en.wikipedia.org/wiki/Quantum_key_distribution)
