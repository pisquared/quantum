# Quantum

This is an experimental repo where I learn quantum computers. 
See corresponding md-s for explanation of each.

## General Resources
* [VIDEO: Feynman's Computer Heuristics Lecture](https://www.youtube.com/watch?v=EKWGGDXe5MA) 
- Nature isn’t classical, dammit, and if you want to make a simulation of 
Nature, you’d better make it quantum mechanical, and by golly it’s a wonderful 
problem, because it doesn’t look so easy.
* [Quantum Computing Series](https://medium.com/@jonathan_hui/qc-quantum-computing-series-10ddd7977abd)
* [VIDEO: Quantum Computing for computer scientists](https://www.youtube.com/watch?v=F_Riqjdh2oM)
* [VIDEO: A Beginner's Guide to Quantum Computing](https://www.youtube.com/watch?v=JRIPV0dPAd4)
