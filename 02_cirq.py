import cirq


def superdense():
    # Pick a qubit.
    q1 = cirq.GridQubit(0, 0)
    q2 = cirq.GridQubit(1, 0)

    # Create a circuit
    circuit = cirq.Circuit.from_ops(
        cirq.H(q1),
        cirq.CNOT(q1, q2),  # Alice prepares the Bell state,
        # #################### Alice sends q2 to Bob,
        # Uncomment below lines depending on what you want to send
        # cirq.I(q1),  # Alice wants to send 00 with superdense coding
        # cirq.X(q1),  # Alice wants to send 01 with superdense coding
        cirq.Z(q1),  # Alice wants to send 10 with superdense coding
        # cirq.X(q1), cirq.Z(q1),  # Alice wants to send 11 with superdense coding
        # ################# Some time later...
        cirq.CNOT(q1, q2),
        cirq.H(q1),  # Bob reverses the encoding
        cirq.measure(q1, key='q1'),  # Measurement.
        cirq.measure(q2, key='q2')  # Measurement.
    )
    print("Circuit:")
    print(circuit)

    # Simulate the circuit several times.
    simulator = cirq.Simulator()
    result = simulator.run(circuit, repetitions=20)
    print("Results:")
    print(result)


if __name__ == "__main__":
    superdense()
