# Learning Google's CIRQ

You need to run `./02_INSTALL_CIRQ.sh` to install this (at least on Ubuntu-based 
distros, having naturally python > 3.5, check with `python3 --version`)

## Sources:
* [Official Tutorial](https://cirq.readthedocs.io/en/stable/tutorial.html)
