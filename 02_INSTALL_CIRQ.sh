#!/bin/bash
sudo apt-get install python3-tk texlive-latex-base latexmk
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt
